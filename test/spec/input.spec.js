import {InputListener} from 'input';
import {Actions} from 'constants';

let documentStub = { addEventListener: function() { } };

function event(keyCode) {
	return { keyCode };
}

describe('input.InputListener', function() {
	let listener;

	beforeEach(function() {
		listener = new InputListener(documentStub);
	});

	it('should keep track of pressed keys', function() {
		listener.onKeyDown(event(Actions.LEFT));
		listener.onKeyDown(event(Actions.RIGHT));

		var keys = listener.getPressedKeys();

		expect(keys.length).toBe(2);
		expect(keys).toContain(Actions.LEFT);
		expect(keys).toContain(Actions.RIGHT);
	});

	it('should keep track of released keys', function() {
		listener.onKeyDown(event(Actions.LEFT));
		listener.onKeyDown(event(Actions.RIGHT));
		listener.onKeyUp(event(Actions.LEFT));

		var keys = listener.getPressedKeys();

		expect(keys.length).toBe(1);
		expect(keys).toContain(Actions.RIGHT);
	});
});
