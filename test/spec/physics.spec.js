import {box, collide} from 'physics';

describe('physics.collide', function() {
	let box1,
		box2_collides_w_box_1,
		box3_does_not_collide_w_box_1,
		box4_adjacent_to_box_1,
		box5_is_malformed;

	beforeAll(function() {
		box1 = box(0, 0, 4, 4);
		box2_collides_w_box_1 = box(3, 3, 5, 5);
		box3_does_not_collide_w_box_1 = box(10, 10, 12, 12);
		box4_adjacent_to_box_1 = box(4, 4, 5, 5);
		box5_is_malformed = { x1: 12 };
	});

	it('should return true when two boxes collide', function() {
		expect(collide(box1, box2_collides_w_box_1)).toBe(true);
	});

	it('should return false when two boxes do not collide', function() {
		expect(collide(box1, box3_does_not_collide_w_box_1)).toBe(false);
	});

	it('should return false when two boxes are adjacent', function() {
		expect(collide(box1, box4_adjacent_to_box_1)).toBe(false);
	});

	it('should raise an error when boxes are malformed', function() {
		expect(() => {
			collide(box1, box5_is_malformed);
		}).toThrow();
	});
});
