import {Actions} from 'constants';

export class InputListener {
	constructor(document) {
		document.addEventListener('keydown', this.onKeyDown.bind(this));
		document.addEventListener('keyup', this.onKeyUp.bind(this));
		this.pressedKeys = [];
	}
	onKeyDown(event) {
		let code = event.keyCode;

		if (this.pressedKeys.indexOf(code) < 0) {
			this.pressedKeys.push(code);
		}
	}
	onKeyUp(event) {
		let code = event.keyCode;
		let pos = this.pressedKeys.indexOf(code);

		if (pos >= 0) {
			this.pressedKeys.splice(pos, 1);
		}
	}
	getPressedKeys() {
		return this.pressedKeys;
	}
}
