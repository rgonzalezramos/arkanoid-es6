export function box(x1, y1, x2, y2) {
	return { x1, y1, x2, y2 };
}

export function collide(box1, box2) {
	// TODO should dissable assertions in prod mode
	for (let box of [box1, box2]) {
		for (let p of ['x1', 'x2', 'y1', 'y2']) {
			if (!box.hasOwnProperty(p) || typeof(box[p]) !== 'number') {
				throw new Error(box + ' has no numberic property ' + p);
			}
		}
	}

	// Not (do not collide is easier)

	return !(
		box1.x2 <= box2.x1 ||
		box1.x1 >= box2.x2 ||
		box1.y2 <= box2.y1 ||
		box1.y1 >= box2.y2
	);
}
