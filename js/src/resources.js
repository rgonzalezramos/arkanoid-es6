import {Sounds} from 'constants';

let AudioContext = window.AudioContext || window.webkitAudioContext;
let context = new AudioContext();

function fetchResource(url, type, callback) {
	let req = new XMLHttpRequest();
	req.open('GET', url);
	req.responseType = type;
	req.onload = () => {
		callback(url, req.response);
	};
	req.send();
}

export class ResourceCache {
	constructor() {
		this.sounds = {};
	}
	load(callback) {
		this.onready = callback;

		for (let key in Sounds) {
			let url = Sounds[key];

			fetchResource(url, 'arraybuffer', (url, response) => {
				context.decodeAudioData(response, (decoded) => {
					this.sounds[Sounds[key]] = decoded;
					this.checkReady();
				});
			});
		}
	}
	checkReady() {
		if (this.ready()) {
			this.onready();
		}
	}
	ready() {
		return Object.keys(this.sounds).length === Object.keys(Sounds).length;
	}
	getSound(key) {
		if (!this.sounds[key]) {
			throw new Error('Sound buffer ' + key + ' not cached');
		}
		return this.sounds[key];
	}
}

export class Mixer {
	constructor(cache) {
		this.cache = cache;
		this.sources = [];
	}
	play(url) {
		this.createSource(url);
	}
	loop(url) {
		this.createSource(url, true);
	}
	createSource(url, loop) {
		let source = context.createBufferSource();
		source.buffer = this.cache.getSound(url);
		source.connect(context.destination);
		source.start(0);
		source.loop = loop || false;
		this.sources.push(source);
	}
	stop() {
		for (let source of this.sources) {
			source.stop();
		}
		this.sources = [];
	}
}
