export let Colors = {
	BLACK: '#000',
	WHITE: '#fff',
	DARK_BLUE: '#009',
	RED: '#f00',
	GRAY: '#aaa',
	GREEN: '#0f0',
	GREENS: ['#0f0', '#0e0', '#0d0', '#0c0', '#0b0', '#0a0']
};

export let Actions = {
	LEFT: 37,
	RIGHT: 39,
	FIRE: 32,
	RESET: 13
};

export let Sounds = {
	BUMP: '/res/sound/bump.wav',
	SOUNDTRACK: '/res/sound/soundtrack.wav',
	ENGINE: '/res/sound/engine.wav'
};
