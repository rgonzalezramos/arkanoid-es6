// Dimensions are specified in pixels
// Speeds are specified in pixels per second
// abbrvs: x, y, w -> width, h -> height, s -> speed, a -> acceleration, xs -> xspeed, ys -> yspeed

import {Actions, Colors, Sounds} from 'constants';
import {box, collide} from 'physics';

class Entity {
	constructor(dim) {
		this.dim = dim;
		this.sounds = [];
	}
	render(ctx) {
		// NO-OP
	}
	update(dt) {
		// NO-OP
	}
	input(key) {

	}
	setSound(sound) {
		this.sounds.push(sound);
	}
	getSounds() {
		let sounds = this.sounds;
		this.sounds = [];
		return sounds;
	}
	collidesWith(other) {
		return collide(this.box(), other.box());
	}
	box() {
		throw new Error('Not implemented');
	}
}

export class Background extends Entity {
	constructor(dim) {
		super(dim);

		let dimension = Math.min(dim.w, dim.h);
		let numberOfStars = 50;
		let maxStarSize = dimension / 300;
		let maxStarSpeed = 100;

		this.stars = [];

		for (let i = 0; i < numberOfStars; i++) {
			this.stars.push({
				x: Math.random() * dim.w,
				y: Math.random() * dim.h,
				r: Math.random() * maxStarSize,
				s: Math.random() * maxStarSpeed
			});
		}
	}
	update(dt) {
		// Update the positions of the stars
		for (let star of this.stars) {
			if (star.y > this.dim.h) {
				// Better than setting to 0 if the browser starts
				// skipping frames
				star.y = star.y % this.dim.h;
				star.x = Math.random() * this.dim.w;
			}
			star.y += star.s * dt;
		}
	}
	render(ctx) {
		// Draw the background
		ctx.fillStyle = Colors.BLACK;
		ctx.fillRect(0, 0, this.dim.w, this.dim.h);

		// Draw the stars
		for (let star of this.stars) {
			// Outer blue circle
			ctx.fillStyle = Colors.DARK_BLUE;
			ctx.beginPath();
			ctx.arc(star.x, star.y, star.r + 1, 0, 2 * Math.PI);
			ctx.fill();

			// Inner white circle
			ctx.beginPath();
			ctx.fillStyle = Colors.WHITE;
			ctx.arc(star.x, star.y, star.r, 0, 2 * Math.PI);
			ctx.fill();
		}

		// Draw borders
		let thickness = 5;
		ctx.fillStyle = Colors.GRAY;
		// Left
		ctx.fillRect(0, 0, thickness, this.dim.h);
		// Right
		ctx.fillRect(this.dim.w - thickness, 0, this.dim.w, this.dim.h);
		// Top
		ctx.fillRect(0, 0, this.dim.w, thickness);
	}
}

export class Platform extends Entity {
	constructor(dim, w) {
		super(dim);
		this.dim = dim;
		this.x = 0.5 * dim.w - 0.5 * w;
		this.h = dim.h / 50;
		this.y = dim.h - 3 * this.h;
		this.w = w;
		this.s = 0;
		this.a = 0;
		this.max_speed = this.w / 1.5;
	}
	box() {
		return box(this.x, this.y, this.x + this.w, this.y + this.h);
	}
	update(dt) {
		let brakes = (this.a * this.s) < 0 ? 5 : 1;
		this.s += this.a * brakes * dt % (Math.abs(this.max_speed));
		this.x += this.s * dt;

		if (this.x <= 0) {
			this.hitWall();
			this.x = 0;
		} else if (this.x + this.w >= this.dim.w) {
			this.hitWall();
			this.x = this.dim.w - this.w;
		}
		this.a = 0;
	}
	hitWall() {
		this.s = -0.5 * this.s;
	}
	render(ctx) {
		ctx.fillStyle = Colors.GRAY;
		ctx.fillRect(this.x, this.y, this.w, this.h);
	}
	input(key) {
		switch (key) {
			case Actions.LEFT:
				this.a = -2 * this.dim.w;
				break;
			case Actions.RIGHT:
				this.a = +2 * this.dim.w;
				break;
		}
	}
}

export class Ball extends Entity {
	constructor(dim, s, platform, bricks) {
		super(dim);
		this.s = s;
		this.r = this.dim.w / 90;
		this.xs = 0;
		this.ys = 0;
		this.attached = true;
		this.platform = platform;
		this.bricks = bricks;
	}
	isOut() {
		return this.y > this.dim.h;
	}
	box() {
		return box(this.x - this.r, this.y - this.r, this.x + this.r, this.y + this.r);
	}
	update(dt) {
		if (this.attached) {
			this.x = this.platform.x + 0.5 * this.platform.w;
			this.y = this.platform.y - this.r;
			return;
		}

		let collided = this.processCollisions();
		if (collided) {
			this.setSound(Sounds.BUMP);
		} else {
			this.x += this.xs * dt;
			this.y += this.ys * dt;
		}
	}
	processCollisions() {
		return 	this.processBorderCollisions() 	||
				this.processPlatformCollision() ||
				this.processBrickCollisions();
	}
	processBorderCollisions() {
		if (this.x - this.r < 0) {
			// Resolve left collision
			this.x = this.r;
			this.xs = -this.xs;
		} else if (this.x + this.r > this.dim.w) {
			// Resolve right collision
			this.x = this.dim.w - this.r;
			this.xs = -this.xs;
		} else if (this.y - this.r < 0) {
			// Resolve top collision
			this.y = this.r;
			this.ys = - this.ys;
		}else {
			return false;
		}
		return true;
	}

	processPlatformCollision() {
		if (this.collidesWith(this.platform)) {
			// The resulting speed vector will be a mapping
			// from [0, 1] to [3, 1] depending on the relative
			// distance of the ball to the center of the platform

			let distanceToTheCenterOfThePlatform = this.x - (this.platform.x + 0.5 * this.platform.w);
			let relativeDistanceToCenterOfThePlatform = Math.min(1, Math.abs(distanceToTheCenterOfThePlatform) / (0.5 * this.platform.w));

			let sign = Math.sign(distanceToTheCenterOfThePlatform);

			let ycomp = -1;
			let xcomp = sign * 3 * relativeDistanceToCenterOfThePlatform;

			let mod = Math.sqrt(ycomp * ycomp + xcomp * xcomp);

			let ynorm = ycomp / mod;
			let xnorm = xcomp / mod;

			this.xs = this.s * xnorm;
			this.ys = this.s * ynorm;

			// Resolve the collision
			this.y = this.platform.y - this.r;


			return true;
		}
		return false;
	}
	processBrickCollisions() {
		for (let brick of this.bricks) {
			if (this.collidesWith(brick)) {
				brick.hit();
				return true;
			}
		}
		return false;
	}
	render(ctx) {
		ctx.beginPath();
		ctx.fillStyle = Colors.RED;
		ctx.arc(this.x, this.y, this.r, 0, 2 * Math.PI);
		ctx.fill();
	}
	input(key) {
		switch (key) {
			case Actions.FIRE:
				if (this.attached) {
					this.attached = false;

					let component = this.s / Math.sqrt(2);
					this.xs = component;
					this.ys = -component;
				}
				break;
			case Actions.RESET:
				this.attached = true;
				break;
		}
	}
}


export class Brick extends Entity {
	constructor(dim, row, col, lifes) {
		super(dim);

		// 1 space, 8 bricks, 1 spacee
		this.w = dim.w / 10;
		this.x = (1 + col) * this.w;

		this.h = this.w / 2;
		this.y = (2 + row) * this.h;

		this.lifes = lifes;
	}
	render(ctx) {
		ctx.fillStyle = Colors.GREENS[this.lifes];
		ctx.fillRect(this.x, this.y, this.w, this.h);
		ctx.strokeRect(this.x, this.y, this.w, this.h);
	}
	box() {
		return box(this.x, this.y, this.x + this.w, this.y + this.h);
	}
	hit() {
		this.lifes--;
	}
	isDead() {
		return this.lifes <= 0;
	}
}
