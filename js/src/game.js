import {Background, Platform, Ball, Brick} from 'entities';
import {InputListener} from 'input';
import {Level} from 'levels';
import {ResourceCache, Mixer} from 'resources';
import {Sounds} from 'constants';

const DEFAULT_WIDTH = 800;
const DEFAULT_HEIGHT = DEFAULT_WIDTH / 4 * 3;

// The game engine
export default class Game {
	constructor(canvas) {
		this.canvas = canvas;
		this.ctx = this.canvas.getContext('2d');

		this.dim = {
			w: DEFAULT_WIDTH,
			h: DEFAULT_HEIGHT
		};

		this.canvas.width = this.dim.w;
		this.canvas.height = this.dim.h;

		this.input = new InputListener(document);
		this.cache = new ResourceCache();
		this.mixer = new Mixer(this.cache);
	}
	start() {
		this.cache.load(this.doStart.bind(this));
	}
	doStart() {
		this.t = Date.now();
		this.initLevel();
		this.run();
	}
	initLevel() {
		this.background = new Background(this.dim);
		// TODO evolve this into a level system
		this.level = new Level(this.dim);

		this.bricks = [];
		var positions = this.level.brickPositions();
		for (let row = 0; row < positions.length; row++) {
			for (let col = 0; col < positions[row].length; col++ ) {
				let lifes = positions[row][col];
				if (lifes > 0) {
					this.bricks.push(new Brick(this.dim, row, col, lifes));
				}
			}
		}

		this.platform = new Platform(this.dim, this.level.platformWidth());
		this.ball = new Ball(this.dim, this.level.ballSpeed(), this.platform, this.bricks);
		this.mixer.loop(Sounds.SOUNDTRACK);

	}
	run() {
		// Measure ellapsed time
		let t = Date.now();
		let dt = 0.001 * (t - this.t);
		this.t = t;

		this.processInput(this.input.getPressedKeys());
		this.update(dt);
		this.render(this.ctx);
		this.playSounds();

		if (this.ball.isOut()) {
			this.mixer.stop();
			this.doStart();
		} else {
			// Wait till the browser is able to draw something
			window.requestAnimationFrame(this.run.bind(this));
		}


	}
	playSounds() {
		for (var sound of this.ball.getSounds()) {
			this.mixer.play(sound);
		}
	}
	processInput(keys) {
		for (let key of keys) {
			this.platform.input(key);
			this.ball.input(key);
		}
	}
	update(dt) {
		this.background.update(dt);
		this.platform.update(dt);
		this.ball.update(dt);
		this.bricks.forEach((brick) => { brick.update(dt); });

		// Remove dead bricks
		for (let i = this.bricks.length - 1; i >= 0 ; i--) {
			if (this.bricks[i].isDead()) {
				this.bricks.splice(i, 1);
			}
		}
	}
	render(ctx) {
		this.background.render(ctx);
		this.platform.render(ctx);
		this.ball.render(ctx);
		this.bricks.forEach((brick) => { brick.render(ctx); });
	}
}
