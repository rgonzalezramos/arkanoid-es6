import Game from 'game';

let canvas = document.getElementById('game');
let game = new Game(canvas);
game.start();
