// Creates all entities and maybe updates them in some way
export class Level {
	constructor(dim) {
		this.dim = dim;
	}
	platformWidth() {
		return this.dim.w / 6;
	}
	ballSpeed() {
		return this.dim.w / 2;
	}
	brickPositions() {
		return [
			[0, 0, 0, 0, 1, 1, 1, 1],
			[2, 2, 2, 1, 1, 1, 1, 1],
			[3, 3, 2, 1, 3, 1, 1, 1],
			[1, 2, 3, 4, 1, 4, 3, 2],
			[1, 2, 3, 4, 1, 4, 3, 2]
		];
	}
}
